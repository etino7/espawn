const puppeteer = require('puppeteer');
const mongoose = require("mongoose");
const Movie = require('./movie');


let scrape = async function getLinks() {

    const URL_TO_SCRAPE = "YOUR_URL";
    var totalData = [];

    const browser = await puppeteer.launch({
        headless: false
    });;
    const page = await browser.newPage();
    await page.goto(URL_TO_SCRAPE);

    await page.setViewport({
        width: 1920,
        height: 1080
    });

    await page.waitFor(700);

    let pages = await getPages(page);

    for (let pg of pages) {

        await page.goto(pg);

        const result = await page.evaluate(() => {
            let data = [];
            let elements = document.querySelectorAll('body > pre > a');

            for (let el of elements) {
                let fullTitle = el.innerText.replace(/\./g, ' ');
                let title = el.innerText.trim();

                if (title.length < 5)
                    continue;

                let url = el.href;

                let formatted = null;
                let year = null;
                let res = null;

                try {
                    let subs = fullTitle.match(/^([ .\w']+?)(\W\d{4})(\W\d+p)?(\W?.*)/i);

                    if (!subs || subs[1] || subs[2] || subs[3]) {
                        formatted = subs[1].trim().toLowerCase();
                        year = parseInt(subs[2].trim());
                        res = subs[3].trim().toLowerCase();
                    }

                } catch (err) {

                }

                let urlInfo = {
                    url,
                    res
                };

                data.push({
                    title,
                    fullTitle,
                    formatted,
                    year,
                    urlInfo
                });

            }



            return data;
        });

        result.map(res => totalData.push(res));

    }


    await browser.close();
    return totalData;

}



async function getPages(page) {
    const result = await page.evaluate(() => {
        let data = [];
        let elements = document.querySelectorAll('body > pre > a');

        let i = 0;
        for (let el of elements) {
            if (i != 0)
                data.push(el.href.trim());

            i++;
        }


        return data;
    });

    return result;
}


function updateOrInsert(film) {

    const DB_URL = 'YOUR_DB_URL';

    if (mongoose.connection.readyState == 0) {
        mongoose.connect(DB_URL);
    }

    let options = {
        upsert: true,
        new: true,
        setDefaultsOnInsert: true
    };

    Movie.findOneAndUpdate({
        name: film.formatted,
        year: film.year
    }, {
        $push: {
            stream: film.urlInfo
        }
    }, options, (err, result) => {
        console.log(err);
        console.log(result);

    });
}


try {
    scrape().then((value) => {
        value.map(v => updateOrInsert(v));
    });
} catch (err) {
    log.e(err);
}