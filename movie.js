const mongoose = require('mongoose');

const movieShema = new mongoose.Schema({
    id: {
        type: Number,
        required: false,
    },
    name: {
        type: String,
    },
    year: {
        type: Number
    },
    stream: [{
        url: {
            type: String,
        },
        resolution: {
            type: String,
            required: true
        }
    }]
});

const Movie = mongoose.model('Movie', movieShema);
module.exports = Movie;